const express = require('express')
const app = express()
const contacto = require('./contacto.json')
const fs = require('fs');
const path = require('path');
const hbs = require('hbs')


app.engine( 'hbs', hbs( {
    extname: 'hbs',
    defaultView: 'default',
    layoutsDir: __dirname + '/views/pages/',
    partialsDir: __dirname + '/views/partials/'
  }));

  

hbs.registerHelper('getCurrentYear', () => new Date().getFullYear())
// con paso de parámetro:
hbs.registerHelper('toUpperCase', text => text.toUpperCase())

//con esto le decimos que tenemos en partials el footer y que le pase el parámetro
hbs.registerPartials(path.join(__dirname, 'views', 'partials'))
app.set('view engine', 'hbs') // clave valor
// llamada a vistas externas dinamicas de hbs
// const hbs = require('hbs')
app.set('view engine', 'hbs'); // clave valor

// llamada a elementos estaticos
const staticRoute = path.join(__dirname, 'public')
app.use('/principal', express.static(staticRoute))
// con  esto lo que hacemos es decirle la ruta para un recurso estatico como puede ser un html

//con esto lo que hacemos es escribir los log en un archivo de texto
app.use(function (req, res, next) {
    var now = new Date().toString()
    let log = `Time: ${now} ${req.method} ${req.url}`  
    fs.appendFile('log.txt', `${log} \n`, (err) => {
        err ? console.log('Ha habido un error') : console.log('Todo ok');
    });  
    next()
});

//mostramos la fecha y donde accedemos
app.use(function (req, res, next) {
    var now = new Date().toString()
    console.log(`Time: ${now} ${req.method} ${req.url}`)
    next()
  })

//mandamos a / un hola mundo
app.get('/', (req, res) => {
    res.send('Hola Mundo!!!!')
})

//página de contactar en /contactar
app.get('/home', (req, res) => {
    res.render('home', {layout: 'default', template: 'home-template'});
});
app.get('/contactar', (req, res) => {
    res.render('contactar.hbs', {
        pageTitle: 'Contactar',
      //  currentYear: new Date().getFullYear()
    })
});

//pagina de contacto que lee un json externo
app.get('/contacto', (req, res) => {
    res.send(contacto)
})

//puerto en escucha y un log avisandonos sobre el puerto en el que se muestra
app.listen(3000, () => {
    console.log('Servidor web arrancado en el puerto 3000')
  })